package com.project.spark.scala

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object App {
  def main(args: Array[String]): Unit = {
      
      val conf = new SparkConf().setAppName("MyApp")
      val sparkContext = new SparkContext(conf)
      
      println("Well i want this value 88% as code coverage")
      for(value <- 0 until 12549) {
        println(s"[INFO] Statement coverage.: ${value+1}.00%")
        }
      
      sparkContext.stop()
    }
  }

